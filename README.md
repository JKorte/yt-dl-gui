# yt-dl-gui

A small shell script with a [yad gui](https://github.com/v1cont/yad) for the [yt-dlp](https://github.com/yt-dlp/yt-dlp).
Copyright AGPL3 only.

![](yt-dl-gui_screenshot.png) ![](yt-dl-gui_screenshot2.png)

## Dependencies
* [yad](https://github.com/v1cont/yad) - Yet another dialog
* [yt-dlp](https://github.com/yt-dlp/yt-dlp) - A youtube-dl fork with additional features and fixes
* [bash](https://www.gnu.org/software/bash/) - the Bourne Again SHell
